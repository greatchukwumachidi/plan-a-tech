﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Runtime.InteropServices;

namespace PlanA.Controllers
{
    [ApiController]
    [Route("/")]
    public class WeatherForecastController : ControllerBase
    {
        public WeatherForecastController()
        {

        }

        private string GetOS(){
            return $"{RuntimeInformation.OSDescription} ";
        }

        [HttpGet]
        public ActionResult Get()
        {
            var data = new {
                Timestamp = DateTimeOffset.Now.ToString("dd MMM yyy hh:mm tt"),
                Hostname = HttpContext.Request.Host.Host.ToString(),
                Engine = GetOS(),
                VisitorIP = HttpContext.Connection.RemoteIpAddress?.MapToIPv4().ToString()
            };

            return Ok(data);
        }
    }
}
