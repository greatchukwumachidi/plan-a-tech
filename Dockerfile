FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -r alpine-x64 --self-contained true /p:PublishTrimmed=true -o ./out

FROM alpine:3.9.4

# Add some libs required by .NET runtime
RUN apk add --no-cache libstdc++ libintl icu

EXPOSE 80
EXPOSE 443

# Copy contents from the Build container
WORKDIR /app
COPY --from=build-env /app/out .

# Run the single App Executable making it to listen on port 80
ENTRYPOINT ["./PlanA", "--urls", "http://0.0.0.0:80"]