# Application Deployment
To deploy the application to the cluster.

## Create a namespace

```
kubectl create ns chidi-plan-a
```

## Switch to the namespace created

```
kubectl config set-context --current --namespace=chidi-plan-a
```

If you have `kubens` installed on your system, you can run this to switch to the namespace
```
kubens chidi-plan-a
```
> Click [here](https://github.com/ahmetb/kubectx) to install kubens 


## Download and install Kustomize...
```
cd .kubernetes/

curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
```
> For more information on how to install Kustomize, click [here](https://github.com/kubernetes-sigs/kustomize)


## Then run this to deploy to the cluster
```
./kustomize build . | kubectl apply -f -
``` 
 
## Access the app by running

```
kubectl get svc -n chidi-plan-a
``` 
Copy the ALB and paste on the browser and there you go
